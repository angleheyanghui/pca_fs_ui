import request from '@/utils/request'

// 查询学生信息列表
export function listStuInfo(query) {
  return request({
    url: '/manage/stuInfo/list',
    method: 'get',
    params: query
  })
}

export function listStuInfoVo(query) {
  return request({
    url: '/manage/stuInfo/listVo',
    method: 'get',
    params: query
  })
}

// 查询学生信息详细
export function test() {
  return request({
    url: '/manage/stuInfo/test',
    method: 'get'
  })
}

// 查询学生信息详细
export function getStuDetail(id) {
  return request({
    url: '/manage/stuInfo/getStuDetail/' + id,
    method: 'get'
  })
}

// 查询学生信息详细
export function getStuInfo(id) {
  return request({
    url: '/manage/stuInfo/' + id,
    method: 'get'
  })
}

// 新增学生信息
export function addStuInfo(data) {
  return request({
    url: '/manage/stuInfo',
    method: 'post',
    data: data
  })
}

// 修改学生信息
export function updateStuInfo(data) {
  return request({
    url: '/manage/stuInfo',
    method: 'put',
    data: data
  })
}

// 删除学生信息
export function delStuInfo(id) {
  return request({
    url: '/manage/stuInfo/' + id,
    method: 'delete'
  })
}

// 导出学生信息
export function exportStuInfo(query) {
  return request({
    url: '/manage/stuInfo/export',
    method: 'get',
    params: query
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/manage/stuInfo/avatar',
    method: 'post',
    data: data
  })
}
