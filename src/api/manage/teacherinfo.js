import request from '@/utils/request'

// 查询教师信息列表
export function listTeacherInfo(query) {
  return request({
    url: '/manage/teacherInfo/list',
    method: 'get',
    params: query
  })
}

export function teacherList() {
  return request({
    url: '/manage/teacherInfo/teacherList',
    method: 'get'
  })
}

export function listTInfoVo(query) {
  return request({
    url: '/manage/teacherInfo/listVo',
    method: 'get',
    params: query
  })
}

// 查询教师信息详细
export function getTeacherInfo(id) {
  return request({
    url: '/manage/teacherInfo/' + id,
    method: 'get'
  })
}

// 新增教师信息
export function addTeacherInfo(data) {
  return request({
    url: '/manage/teacherInfo',
    method: 'post',
    data: data
  })
}

// 修改教师信息
export function updateTeacherInfo(data) {
  return request({
    url: '/manage/teacherInfo',
    method: 'put',
    data: data
  })
}

// 删除教师信息
export function delTeacherInfo(id) {
  return request({
    url: '/manage/teacherInfo/' + id,
    method: 'delete'
  })
}

// 导出教师信息
export function exportTeacherInfo(query) {
  return request({
    url: '/manage/teacherInfo/export',
    method: 'get',
    params: query
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/manage/teacherInfo/avatar',
    method: 'post',
    data: data
  })
}

// 查询学生信息详细
export function getTDetail(id) {
  return request({
    url: '/manage/teacherInfo/getTDetail/' + id,
    method: 'get'
  })
}
