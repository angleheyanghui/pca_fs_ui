import request from '@/utils/request'

// 查询学生数量
export function stuCount() {
  return request({
    url: '/manage/countData/stuCount',
    method: 'get',
  })
}
export function userCount() {
  return request({
    url: '/manage/countData/userCount',
    method: 'get',
  })
}
export function teacherCount() {
  return request({
    url: '/manage/countData/teacherCount',
    method: 'get',
  })
}
export function actCount() {
  return request({
    url: '/manage/countData/actCount',
    method: 'get',
  })
}
export function communityCount() {
  return request({
    url: '/manage/countData/communityCount',
    method: 'get',
  })
}
export function collegeCount() {
  return request({
    url: '/manage/countData/collegeCount',
    method: 'get',
  })
}

//学院社团统计
export function ccCount() {
  return request({
    url: '/manage/countData/ccCount',
    method: 'get',
  })
}

//学院活动统计
export function cActCount() {
  return request({
    url: '/manage/countData/cActCount',
    method: 'get',
  })
}

//学院入团学生统计
export function cUserCount() {
  return request({
    url: '/manage/countData/cUserCount',
    method: 'get',
  })
}

//学院学生统计
export function cStuCount() {
  return request({
    url: '/manage/countData/cStuCount',
    method: 'get',
  })
}

//一等奖
export function cFirstCount() {
  return request({
    url: '/manage/countData/cFirstCount',
    method: 'get',
  })
}
//二等奖
export function cSecondCount() {
  return request({
    url: '/manage/countData/cSecondCount',
    method: 'get',
  })
}
//三等奖
export function cThirdCount() {
  return request({
    url: '/manage/countData/cThirdCount',
    method: 'get',
  })
}



