import request from '@/utils/request'

// 查询财务数据仓列表
export function listDatabase(query) {
  return request({
    url: '/manage/database/list',
    method: 'get',
    params: query
  })
}

// 查询财务数据仓详细
export function getDatabase(id) {
  return request({
    url: '/manage/database/' + id,
    method: 'get'
  })
}

// 新增财务数据仓
export function addDatabase(data) {
  return request({
    url: '/manage/database',
    method: 'post',
    data: data
  })
}

// 新增父级
export function addParent(data) {
  return request({
    url: '/manage/database/addParent',
    method: 'post',
    data: data
  })
}

// 查询下拉树结构
export function getTree() {
  return request({
    url: '/manage/database/getTree',
    method: 'get'
  })
}

// 修改财务数据仓
export function updateDatabase(data) {
  return request({
    url: '/manage/database',
    method: 'put',
    data: data
  })
}

// 删除财务数据仓
export function delDatabase(id) {
  return request({
    url: '/manage/database/' + id,
    method: 'delete'
  })
}

// 导出财务数据仓
export function exportDatabase(query) {
  return request({
    url: '/manage/database/export',
    method: 'get',
    params: query
  })
}
