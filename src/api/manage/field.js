import request from '@/utils/request'

// 查询字段信息列表
export function listField(query) {
  return request({
    url: '/manage/field/list',
    method: 'get',
    params: query
  })
}

// 查询字段信息列表
export function getList(query) {
  return request({
    url: '/manage/field/getList',
    method: 'get',
    params: query
  })
}

export function getFieldList(tabNameEn) {
  return request({
    url: '/manage/field/getFieldList/' + tabNameEn,
    method: 'get'
  })
}
export function getTabData(query) {
  return request({
    url: '/manage/field/getTabData',
    method: 'get',
    params: query
  })
}
export function getFieldAndData(query) {
  return request({
    url: '/manage/field/getFieldAndData',
    method: 'get',
    params: query
  })
}

// 查询字段信息详细
export function getField(id) {
  return request({
    url: '/manage/field/' + id,
    method: 'get'
  })
}

export function haveData(tabNameEn) {
  return request({
    url: '/manage/field/haveData/' + tabNameEn,
    method: 'get'
  })
}

// 新增字段信息
export function addField(data) {
  return request({
    url: '/manage/field',
    method: 'post',
    data: data
  })
}

export function importDataToTab(data) {
  return request({
    url: '/manage/field/importDataToTab',
    method: 'post',
    data: data
  })
}

// 修改字段信息
export function updateField(data) {
  return request({
    url: '/manage/field',
    method: 'put',
    data: data
  })
}

// 删除字段信息
export function delField(id) {
  return request({
    url: '/manage/field/' + id,
    method: 'delete'
  })
}

// 导出字段信息
export function exportField(query) {
  return request({
    url: '/manage/field/export',
    method: 'get',
    params: query
  })
}