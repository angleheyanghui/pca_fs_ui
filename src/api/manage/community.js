import request from '@/utils/request'

// 查询社团列表
export function listCommunity(query) {
  return request({
    url: '/manage/community/list',
    method: 'get',
    params: query
  })
}

export function listVo(query) {
  return request({
    url: '/manage/community/listVo',
    method: 'get',
    params: query
  })
}

// 查询社团详细
export function getCommunity(id) {
  return request({
    url: '/manage/community/' + id,
    method: 'get'
  })
}

export function getCommunityByUser(userId) {
  return request({
    url: '/manage/community/getCommunityByUser/' + userId,
    method: 'get'
  })
}

// 新增社团
export function addCommunity(data) {
  return request({
    url: '/manage/community',
    method: 'post',
    data: data
  })
}
// 申请加入社团
export function joinCommunity(data) {
  return request({
    url: '/manage/community/joinCommunity',
    method: 'post',
    data: data
  })
}

// 修改社团
export function updateCommunity(data) {
  return request({
    url: '/manage/community',
    method: 'put',
    data: data
  })
}

// 删除社团
export function delCommunity(id) {
  return request({
    url: '/manage/community/' + id,
    method: 'delete'
  })
}

// 同意加入社团
export function agreeJoinCmt(id) {
  return request({
    url: '/manage/community/agreeJoinCmt/' + id,
    method: 'post'
  })
}
// 拒绝加入社团
export function refuseJoinCmt(id) {
  return request({
    url: '/manage/community/refuseJoinCmt/' + id,
    method: 'post'
  })
}

// 导出社团
export function exportCommunity(query) {
  return request({
    url: '/manage/community/export',
    method: 'get',
    params: query
  })
}
//获取社团申请待审批信息
export function getWaitApprovalByUser(userId) {
  return request({
    url: '/manage/community/getWaitApprovalByUser/' + userId,
    method: 'get'
  })
}

//获取社团申已同意信息
export function getAgreeByUser(userId) {
  return request({
    url: '/manage/community/getAgreeByUser/' + userId,
    method: 'get'
  })
}
//获取社团申请已拒绝信息
export function getRefuseByUser(userId) {
  return request({
    url: '/manage/community/getRefuseByUser/' + userId,
    method: 'get'
  })
}
