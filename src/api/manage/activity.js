import request from '@/utils/request'

// 查询活动列表
export function listActivity(query) {
  return request({
    url: '/manage/activity/list',
    method: 'get',
    params: query
  })
}
export function listVo(query) {
  return request({
    url: '/manage/activity/listVo',
    method: 'get',
    params: query
  })
}

// 查询活动详细
export function getActivity(id) {
  return request({
    url: '/manage/activity/' + id,
    method: 'get'
  })
}

// 新增活动
export function addActivity(data) {
  return request({
    url: '/manage/activity',
    method: 'post',
    data: data
  })
}

//活动打分
export function scoring(data) {
  return request({
    url: '/manage/activity/scoring',
    method: 'post',
    data: data
  })
}



//报名参加活动
export function joinAct(id) {
  return request({
    url: '/manage/activity/joinAct',
    method: 'post',
    data: id
  })
}

// 修改活动
export function updateActivity(data) {
  return request({
    url: '/manage/activity',
    method: 'put',
    data: data
  })
}

export function updateActState(data) {
  return request({
    url: '/manage/activity/updateActState',
    method: 'put',
    data: data
  })
}

// 删除活动
export function delActivity(id) {
  return request({
    url: '/manage/activity/' + id,
    method: 'delete'
  })
}

// 导出活动
export function exportActivity(query) {
  return request({
    url: '/manage/activity/export',
    method: 'get',
    params: query
  })
}

// 查询活动参加者
export function getActUsers(id) {
  return request({
    url: '/manage/activity/getActUsers/' + id,
    method: 'get'
  })
}

//获取活动结果 通过学院ID
export function getCollegeActResult(query) {
  return request({
    url: '/manage/activity/getCollegeActResult',
    method: 'get',
    params: query
  })
}

//获取所有活动结果
export function getAllActResult(query) {
  return request({
    url: '/manage/activity/getAllActResult',
    method: 'get',
    params: query
  })
}