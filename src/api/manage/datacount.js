import request from '@/utils/request'

// 查询便利蜂分店信息列表
export function storeCountByYear() {
  return request({
    url: '/manage/datacount/storeCountByYear',
    method: 'get'
  })
}
export function storeCountByProvince() {
  return request({
    url: '/manage/datacount/storeCountByProvince',
    method: 'get'
  })
}
export function storeCountBySum() {
  return request({
    url: '/manage/datacount/storeCountBySum',
    method: 'get'
  })
}
export function storeIncomeByTime(query) {
  return request({
    url: '/manage/datacount/storeIncomeByTime',
    method: 'get',
    params: query
  })
}

export function storeCostByTime(query) {
  return request({
    url: '/manage/datacount/storeCostByTime',
    method: 'get',
    params: query
  })
}

export function analyzeByTime(query) {
  return request({
    url: '/manage/datacount/analyzeByTime',
    method: 'get',
    params: query
  })
}
export function analyzeInfo(query) {
  return request({
    url: '/manage/datacount/analyzeInfo',
    method: 'get',
    params: query
  })
}

export function qzxhData(query) {
  return request({
    url: '/manage/datacount/qzxhData',
    method: 'get',
    params: query
  })
}
export function xfcData(query) {
  return request({
    url: '/manage/datacount/xfcData',
    method: 'get',
    params: query
  })
}
export function tzzjzData(query) {
  return request({
    url: '/manage/datacount/tzzjzData',
    method: 'get',
    params: query
  })
}
export function zfcjs(query) {
  return request({
    url: '/manage/datacount/zfcjs',
    method: 'get',
    params: query
  })
}
export function zcf(query) {
  return request({
    url: '/manage/datacount/zcf',
    method: 'get',
    params: query
  })
}
export function tzzxlData(query) {
  return request({
    url: '/manage/datacount/tzzxlData',
    method: 'get',
    params: query
  })
}

export function storeDetailCount(query) {
  return request({
    url: '/manage/datacount/storeDetailCount',
    method: 'get',
    params: query
  })
}

// 查询便利蜂分店信息详细
export function getStoreinfo(id) {
  return request({
    url: '/manage/storeinfo/' + id,
    method: 'get'
  })
}

// 新增便利蜂分店信息
export function addStoreinfo(data) {
  return request({
    url: '/manage/storeinfo',
    method: 'post',
    data: data
  })
}

// 修改便利蜂分店信息
export function updateStoreinfo(data) {
  return request({
    url: '/manage/storeinfo',
    method: 'put',
    data: data
  })
}

// 删除便利蜂分店信息
export function delStoreinfo(id) {
  return request({
    url: '/manage/storeinfo/' + id,
    method: 'delete'
  })
}

// 导出便利蜂分店信息
export function exportStoreinfo(query) {
  return request({
    url: '/manage/storeinfo/export',
    method: 'get',
    params: query
  })
}
