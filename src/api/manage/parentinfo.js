import request from '@/utils/request'

// 查询父母信息列表
export function listParentinfo(query) {
  return request({
    url: '/manage/parentinfo/list',
    method: 'get',
    params: query
  })
}

// 查询父母信息详细
export function getParentinfo(id) {
  return request({
    url: '/manage/parentinfo/' + id,
    method: 'get'
  })
}

// 新增父母信息
export function addParentinfo(curParentInfo) {
  return request({
    url: '/manage/parentinfo/save',
    method: 'put',
    params: curParentInfo
  })
}

// 修改父母信息
export function updateParentinfo(data) {
  return request({
    url: '/manage/parentinfo',
    method: 'put',
    params: data
  })
}



// 删除父母信息
export function delParentinfo(id) {
  return request({
    url: '/manage/parentinfo/' + id,
    method: 'delete'
  })
}

// 导出父母信息
export function exportParentinfo(query) {
  return request({
    url: '/manage/parentinfo/export',
    method: 'get',
    params: query
  })
}
